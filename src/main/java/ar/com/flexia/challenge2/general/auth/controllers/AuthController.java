package ar.com.flexia.challenge2.general.auth.controllers;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import ar.com.flexia.challenge2.general.auth.models.Credentials;
import ar.com.flexia.challenge2.general.auth.models.Session;
import ar.com.flexia.challenge2.general.auth.models.User;
import ar.com.flexia.challenge2.general.auth.services.UserService;

@RestController
@RequestMapping(path = "/auth")
public class AuthController {

	private UserService userService;
	
	/**
	 * @param userService
	 */
	public AuthController(UserService userService) {
		super();
		this.userService = userService;
	}

	@Operation(summary = "Iniciar sesión", description = "Recibe las credenciales de un operador o administrador y devuelve su información de sesión.", tags = {"Autenticación"})
	@PostMapping(path = "/login")
	public Session login(@RequestBody Credentials creds) {
		return userService.login(creds.getEmail(), creds.getPassword());
	}

	@Operation(summary = "Información de usuario", description = "Recibe el token de sesión y devuelve la información de usuario.", tags = {"Autenticación"})
	@GetMapping(path = "/user")
	public User getCurrentUser(Authentication auth) {
		String authName = auth.getName();

		return userService.findUser(authName).orElseThrow(() ->
			new UsernameNotFoundException("Usuario no encontrado.")
		);
	}
}
