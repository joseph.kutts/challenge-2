package ar.com.flexia.challenge2.general.auth.models;

public class Session {

	private String token;
	
	private User user;

	/**
	 * @param token
	 * @param user
	 */
	public Session(String token, User user) {
		super();
		this.token = token;
		this.user = user;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}
}
