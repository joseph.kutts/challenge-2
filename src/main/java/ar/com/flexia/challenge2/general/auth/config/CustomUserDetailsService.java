package ar.com.flexia.challenge2.general.auth.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ar.com.flexia.challenge2.general.auth.models.User;
import ar.com.flexia.challenge2.general.auth.services.UserService;

import java.util.Optional;

/**
 * {@link UserDetailsService} implementation that looks up a {@link User} in the database
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {
	
	private static final Logger LOG = LoggerFactory.getLogger(CustomUserDetailsService.class);

	private UserService userService;
	
	/**
	 * @param userService
	 */
	public CustomUserDetailsService(UserService userService) {
		super();
		this.userService = userService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) {
		LOG.debug("loading user {}", username);
		Optional<User> opt = userService.findUser(username);
		if (!opt.isPresent()) {
			LOG.debug("user not found: {}", username);
			throw new UsernameNotFoundException("Nombre de usuario no encontrado, usuario: " + username);
		}
		return opt.get();
	}
}
