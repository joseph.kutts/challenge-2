package ar.com.flexia.challenge2.general.auth.services.implementations;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.interfaces.DecodedJWT;

import ar.com.flexia.challenge2.general.auth.config.JWTConfig;
import ar.com.flexia.challenge2.general.auth.services.JWTService;

/**
 * {@link JWTService} implementation based on Auth0 lib (https://github.com/auth0/java-jwt)
 */
@Service
public class Auth0JWTService implements JWTService {

	private JWTConfig config;
	
	private JWTVerifier verifier;

	/**
	 */
	@Autowired
	public Auth0JWTService(JWTConfig config) {
		this.config = config;
		this.verifier = JWT.require(config.getAlgorithm()).build();
	}

	@Override
	public String issueToken(String subject, List<String> authorities) {
		String jwt = JWT.create()
				.withSubject(subject)
				.withExpiresAt(Date.from(Instant.now().plusSeconds(config.getExpiration())))
				.withArrayClaim(JWTConfig.AUTHORITIES_CLAIM, authorities.toArray(new String[0]))
				.sign(config.getAlgorithm());
		return JWTConfig.TOKEN_PREFIX + jwt;
	}
	
	@Override
	public DecodedJWT verify(String jwt) {
		jwt = jwt.replace(JWTConfig.TOKEN_PREFIX, "").trim();
		return verifier.verify(jwt);
	}
}
