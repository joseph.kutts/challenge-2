package ar.com.flexia.challenge2.general.auth.models;

public class Credentials {

	private String email;
	private String password;
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
}
