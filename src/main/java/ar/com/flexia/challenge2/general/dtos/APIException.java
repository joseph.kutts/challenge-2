package ar.com.flexia.challenge2.general.dtos;

public class APIException extends RuntimeException {

    private String code;

    public APIException(String code, String message) {
        super(message);
        this.code = code;
    }

    public APIException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
