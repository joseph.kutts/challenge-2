package ar.com.flexia.challenge2.general.auth.services;

import java.util.List;

import com.auth0.jwt.interfaces.DecodedJWT;

public interface JWTService {

	/**
	 * Issue a JWT for the given subject and authorities
	 * @param subject
	 * @param authorities
	 * @return JWT string with configured prefix already appended
	 */
	public String issueToken(String subject, List<String> authorities);
	
	/**
	 * Verify JWT
	 * @param jwt
	 * @return decoded token
	 */
	public DecodedJWT verify(String jwt);
}
