package ar.com.flexia.challenge2.general.auth.services;

import java.util.Optional;

import ar.com.flexia.challenge2.general.auth.models.Session;
import ar.com.flexia.challenge2.general.auth.models.User;

public interface UserService {

	public Optional<User> findUser(String username);

	public Session login(String email, String password);

}
