package ar.com.flexia.challenge2.general.dtos;

/**
 * Mensaje general de confirmación a nivel API
 */
public class APIConfirmation {
    private String code;
    private String message;
    private Object entity;

    /**
     * @param code
     * @param message
     */
    public APIConfirmation(String code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * @param code
     * @param message
     * @param entity
     */
    public APIConfirmation(String code, String message, Object entity) {
        this.code = code;
        this.message = message;
        this.entity = entity;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Object getEntity() {
        return entity;
    }

}
