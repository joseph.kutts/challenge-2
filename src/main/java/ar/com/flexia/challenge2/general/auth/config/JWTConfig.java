package ar.com.flexia.challenge2.general.auth.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.auth0.jwt.algorithms.Algorithm;

/**
 * Clase de configuración para tokens JWT usando librería de Auth0 (https://github.com/auth0/java-jwt)
 */
@Configuration
public class JWTConfig {
	
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_NAME = "Authorization";
	public static final String AUTHORITIES_CLAIM = "authorities";
	
	/**
	 * Algoritmo para firmado de tokens
	 */
	private Algorithm algorithm = null;
	
	private String secret;
	
	/**
	 * Expiración de tokens, en segundos
	 */
	private Long expiration;
	
	/**
	 * @param secret
	 * @param expiration
	 */
	public JWTConfig(@Value("${security.jwt.secret:my-super-default-secret}") String secret,
			@Value("${security.jwt.exp:86400}") Long expiration) {
		this.algorithm = Algorithm.HMAC512(secret);
		this.secret = secret;
		this.expiration = expiration;
	}
	
	/**
	 * @return the secret
	 */
	public String getSecret() {
		return secret;
	}

	/**
	 * @return the expiration
	 */
	public Long getExpiration() {
		return expiration;
	}

	/**
	 * @return the algorithm
	 */
	public Algorithm getAlgorithm() {
		return algorithm;
	}
}
