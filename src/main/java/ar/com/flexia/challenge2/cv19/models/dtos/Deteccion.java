package ar.com.flexia.challenge2.cv19.models.dtos;

import ar.com.flexia.challenge2.cv19.models.Paciente;

public class Deteccion {
    private String cliente;

    private String detección;

    private Ubicacion ubicacion;

    private Paciente paciente;


    public Deteccion() {}


    public String getCliente() {
        return cliente;
    }

    public String getDetección() {
        return detección;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public void setDetección(String detección) {
        this.detección = detección;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
}
