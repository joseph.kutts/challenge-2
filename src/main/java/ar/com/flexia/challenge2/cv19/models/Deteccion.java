package ar.com.flexia.challenge2.cv19.models;

import ar.com.flexia.challenge2.cv19.models.dtos.Ubicacion;

import javax.persistence.*;

@Entity
public class Deteccion {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_deteccion")
    private Long id;

    private String cliente;

    private String deteccion;

    @Embedded
    private Ubicacion ubicacion;

    @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "paciente.id")
    private Paciente paciente;

    public Deteccion() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getDetección() {
        return deteccion;
    }

    public void setDetección(String deteccion) {
        this.deteccion = deteccion;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }
}
