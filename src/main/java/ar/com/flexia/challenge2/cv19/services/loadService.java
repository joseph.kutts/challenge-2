package ar.com.flexia.challenge2.cv19.services;

import ar.com.flexia.challenge2.cv19.models.Deteccion;
import ar.com.flexia.challenge2.general.dtos.APIConfirmation;
import org.springframework.security.core.Authentication;

public interface loadService {

    APIConfirmation addDeteccion(Deteccion deteccion, Authentication auth);
}
