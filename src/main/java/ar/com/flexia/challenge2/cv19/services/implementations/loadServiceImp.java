package ar.com.flexia.challenge2.cv19.services.implementations;

import ar.com.flexia.challenge2.cv19.models.Deteccion;
import ar.com.flexia.challenge2.cv19.models.DeteccionRepository;
import ar.com.flexia.challenge2.cv19.models.Paciente;
import ar.com.flexia.challenge2.cv19.models.PacienteRepository;
import ar.com.flexia.challenge2.cv19.services.loadService;
import ar.com.flexia.challenge2.general.auth.models.User;
import ar.com.flexia.challenge2.general.auth.models.UserRepository;
import ar.com.flexia.challenge2.general.dtos.APIConfirmation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class loadServiceImp implements loadService {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private DeteccionRepository detRepo;

    @Autowired
    private PacienteRepository pacRepo;

    public loadServiceImp() {}

    @Override
    public APIConfirmation addDeteccion(Deteccion deteccion, Authentication auth) {
        Optional<User> user = userRepo.findByEmail(auth.getPrincipal().toString());

        if (user.isPresent() && user.get().getName() != null) {
            deteccion.setCliente(user.get().getName());
        }

        Optional<Paciente> paciente = pacRepo.findByDni(deteccion.getPaciente().getDni());

        if (paciente.isPresent()) {
            deteccion.setPaciente(paciente.get());
        }

        Deteccion finalObject = detRepo.save(deteccion);

        return new APIConfirmation("200", "Detección guardada en la base de datos.", finalObject);
    }
}
