package ar.com.flexia.challenge2.cv19.models.dtos;

import java.util.Optional;

public class FechasDH {

    private String desde;

    private String hasta;

    private String provincia;

    private String municipio;


    public FechasDH() {}


    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getHasta() {
        return hasta;
    }

    public void setHasta(String hasta) {
        this.hasta = hasta;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }
}
