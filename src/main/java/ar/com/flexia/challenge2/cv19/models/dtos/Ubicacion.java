package ar.com.flexia.challenge2.cv19.models.dtos;

import javax.persistence.Embeddable;

@Embeddable
public class Ubicacion {

    private String provincia;

    private String municipio;


    public Ubicacion() {}


    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }
}
