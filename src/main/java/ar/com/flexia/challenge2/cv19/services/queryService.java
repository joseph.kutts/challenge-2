package ar.com.flexia.challenge2.cv19.services;

import ar.com.flexia.challenge2.cv19.models.Deteccion;
import ar.com.flexia.challenge2.cv19.models.dtos.FechasDH;
import ar.com.flexia.challenge2.general.dtos.APIConfirmation;

import java.util.List;

public interface queryService {

    List<Deteccion> getNdetecciones(Integer ultimas);

    List<Deteccion> getDeteccionesDH(FechasDH fechas);

    List<Deteccion> getDeteccionDHprovincia(FechasDH fechas, String provincia);

    List<Deteccion> getDeteccionDHmunicipio(FechasDH fechas, String municipio);
}
