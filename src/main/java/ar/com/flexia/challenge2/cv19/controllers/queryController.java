package ar.com.flexia.challenge2.cv19.controllers;

import ar.com.flexia.challenge2.cv19.models.Deteccion;
import ar.com.flexia.challenge2.cv19.models.dtos.FechasDH;
import ar.com.flexia.challenge2.cv19.services.loadService;
import ar.com.flexia.challenge2.cv19.services.queryService;
import ar.com.flexia.challenge2.general.dtos.APIConfirmation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import ar.com.flexia.challenge2.cv19.services.queryService;

import java.util.List;

@RestController
@RequestMapping(path = "/api/query/")
@Secured({"ADMIN"})
public class queryController {

    @Autowired
    private queryService queryService;

    public queryController() {}

    @Operation(summary = "Obtener las ultimas n detecciones.", description = "Permite obtener las ultimas n detecciones.", tags = {"CV19-QUERY"})
    @GetMapping(path = "/detecciones")
    public List<Deteccion> getNdetecciones(@RequestParam Integer ultimas) {
        return queryService.getNdetecciones(ultimas);
    }

    @Operation(summary = "Obtener casos desde x, hasta y.", description = "Permite obtener la cantidad de casos en un rango de fechas (fecha desde y fecha hasta).", tags = {"CV19-QUERY"})
    @GetMapping(path = "/detecciones-dh")
    public List<Deteccion> getDeteccionesDH(@RequestBody FechasDH fechas) {
        return queryService.getDeteccionesDH(fechas);
    }

    @Operation(summary = "Segun la provincia, obtener casos desde x, hasta y.", description = "Permite obtener la cantidad de casos en un rango de fechas (fecha desde y fecha hasta) para una provincia dada.", tags = {"CV19-QUERY"})
    @GetMapping(path = "/detecciones-dh/provincia")
    public List<Deteccion> getDeteccionDHprovincia(@RequestBody FechasDH fechas, @RequestParam String nombre) {
        return queryService.getDeteccionDHprovincia(fechas, nombre);
    }

    @Operation(summary = "Segun el municipio, obtener casos desde x, hasta y.", description = "Permite obtener la cantidad de casos en un rango de fechas (fecha desde y fecha hasta) para un municipio dado.", tags = {"CV19-QUERY"})
    @GetMapping(path = "/detecciones-dh/municipio")
    public List<Deteccion> getDeteccionDHmunicipio(@RequestBody FechasDH fechas, @RequestParam String nombre) {
        return queryService.getDeteccionDHmunicipio(fechas, nombre);
    }


}
