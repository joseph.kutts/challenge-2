package ar.com.flexia.challenge2.cv19.services.implementations;

import ar.com.flexia.challenge2.cv19.models.Deteccion;
import ar.com.flexia.challenge2.cv19.models.DeteccionRepository;
import ar.com.flexia.challenge2.cv19.models.dtos.FechasDH;
import ar.com.flexia.challenge2.cv19.services.queryService;
import ar.com.flexia.challenge2.general.dtos.APIConfirmation;

import ar.com.flexia.challenge2.general.dtos.APIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class queryServiceImp implements queryService {

    @Autowired
    private DeteccionRepository detRepo;

    public queryServiceImp() {}

    @Override
    public List<Deteccion> getNdetecciones(Integer ultimas) {
        Pageable pageable = PageRequest.of(0, ultimas, Sort.by(Sort.Order.desc("deteccion"), Sort.Order.desc("id")));
        Page<Deteccion> deteccionesPage = detRepo.findAll(pageable);
        List<Deteccion> detecciones = deteccionesPage.get().collect(Collectors.toList());

        return detecciones;
    }

    @Override
    public List<Deteccion> getDeteccionesDH(FechasDH fechas) {
        // Comprobacion manual de las fechas
        try {
            Date desde = java.sql.Date.valueOf(fechas.getDesde());
            Date hasta = java.sql.Date.valueOf(fechas.getHasta());

            if (desde.after(hasta)) {
                throw new APIException("400", "El intervalo de tiempo indicado no es valido.");
            }

            List<Deteccion> detecciones = detRepo.findByDeteccionBetween(desde.toString(), hasta.toString());

            return detecciones;

        } catch (IllegalArgumentException exception) {
            throw new APIException("400", "Una de las fechas ingresadas no es valida.");

        }
    }

    @Override
    public List<Deteccion> getDeteccionDHprovincia(FechasDH fechas, String provincia) {
        // Comprobacion manual de las fechas
        try {
            Date desde = java.sql.Date.valueOf(fechas.getDesde());
            Date hasta = java.sql.Date.valueOf(fechas.getHasta());

            if (desde.after(hasta)) {
                throw new APIException("400", "El intervalo de tiempo indicado no es valido.");
            }

            if (fechas.getProvincia() == null) {
                throw new APIException("400", "Debes especificar una provincia.");
            }

            List<Deteccion> detecciones = detRepo.findByUbicacion_ProvinciaAndDeteccionBetween(fechas.getProvincia(), desde.toString(), hasta.toString());

            return detecciones;

        } catch (IllegalArgumentException exception) {
            throw new APIException("400", "Una de las fechas ingresadas no es valida.");

        }
    }

    @Override
    public List<Deteccion> getDeteccionDHmunicipio(FechasDH fechas, String municipio) {
        // Comprobacion manual de las fechas
        try {
            Date desde = java.sql.Date.valueOf(fechas.getDesde());
            Date hasta = java.sql.Date.valueOf(fechas.getHasta());

            if (desde.after(hasta)) {
                throw new APIException("400", "El intervalo de tiempo indicado no es valido.");
            }

            if (fechas.getMunicipio() == null) {
                throw new APIException("400", "Debes especificar un municipio.");
            }

            List<Deteccion> detecciones = detRepo.findByUbicacion_MunicipioAndDeteccionBetween(municipio, desde.toString(), hasta.toString());

            return detecciones;

        } catch (IllegalArgumentException exception) {
            throw new APIException("400", "Una de las fechas ingresadas no es valida.");

        }
    }


}
