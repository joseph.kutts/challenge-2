package ar.com.flexia.challenge2.cv19.models;

import ar.com.flexia.challenge2.cv19.models.dtos.Ubicacion;
import ar.com.flexia.challenge2.general.auth.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface DeteccionRepository extends JpaRepository<Deteccion, Long> {

    public List<Deteccion> findByDeteccionBetween(String fromDate, String toDate);

    public List<Deteccion> findByUbicacion_ProvinciaAndDeteccionBetween(String provincia, String fromDate, String toDate);

    public List<Deteccion> findByUbicacion_MunicipioAndDeteccionBetween(String municipio, String fromDate, String toDate);


}
