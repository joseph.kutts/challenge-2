package ar.com.flexia.challenge2.cv19.controllers;

import ar.com.flexia.challenge2.cv19.models.Deteccion;
import ar.com.flexia.challenge2.cv19.services.loadService;
import ar.com.flexia.challenge2.general.dtos.APIConfirmation;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/load/")
@Secured({"CLIENTE", "ADMIN"})
public class loadController {

    @Autowired
    private loadService loadService;

    public loadController() {}


    @Operation(summary = "Añadir caso detectado", description = "Permite agregar un caso detectado.", tags = {"CV19-LOAD"})
    @PostMapping(path = "/deteccion")
    // @PreAuthorize("@APIVerification.canAct(#restaurantId, authentication.principal)")
    public APIConfirmation addDeteccion(@RequestBody Deteccion deteccion, Authentication auth) {
        return loadService.addDeteccion(deteccion, auth);
    }
}
